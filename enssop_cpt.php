<?php
//https://wordpress.org/support/article/managing-plugins/

/**
 * Plugin Name: Enssop CPT
 * Description : Génère un type de publication personnalisé pour les recettes du site
 */

// https://fr.wordpress.org/support/article/post-types/#posts
// https://codex.wordpress.org/Function_Reference/register_post_type

include 'enssop_taxo.php' ;

 if(! defined('enssop_cpt')){
     function enssop_cpt(){


        /**
         * Custom Post Type : Recette
         */
        $labels = array(
        "name"                  => __( "Recettes", "enssop" ),
		"singular_name"         => __( "Recette", "enssop" ),
		"menu_name"             => __( "Recettes", "enssop" ),
		"all_items"             => __( "Toutes les recettes", "enssop" ),
		"add_new"               => __( "Ajouter une recette", "enssop" ),
		"add_new_item"          => __( "Ajouter une nouvelle recette", "enssop" ),
		"edit_item"             => __( "Ajouter la recette", "enssop" ),
		"new_item"              => __( "Nouvelle recette", "enssop" ),
		"view_item"             => __( "Voir la recette", "enssop" ),
		"view_items"            => __( "Voir les recettes", "enssop" ),
		"search_items"          => __( "Chercher une recette", "enssop" ),
		"not_found"             => __( "Pas de recette trouvé", "enssop" ),
		"not_found_in_trash"    => __( "Pas de recette trouvé dans la corbeille", "enssop" ),
		"featured_image"        => __( "Image mise en avant pour ce recette", "enssop" ),
		"set_featured_image"    => __( "Définir l'image mise en avant pour ce recette", "enssop" ),
		"remove_featured_image" => __( "Supprimer l'image mise en avant pour ce recette", "enssop" ),
		"use_featured_image"    => __( "Utiliser comme image mise en avant pour ce recette", "enssop" ),
		"archives"              => __( "Type de recette", "enssop" ),
		"insert_into_item"      => __( "Ajouter au recette", "enssop" ),
		"uploaded_to_this_item" => __( "Ajouter au recette", "enssop" ),
		"filter_items_list"     => __( "Filtrer la liste de recettes", "enssop" ),
		"items_list_navigation" => __( "Naviguer dans la liste de recettes", "enssop" ),
		"items_list"            => __( "Liste de recettes", "enssop" ),
		"attributes"            => __( "Paramètres du recette", "enssop" ),
		"name_admin_bar"        => __( "recette", "enssop" ),
        );

        $args= array(
            "label"     => __('Recette', 'enssop'),
            "labels"    => $labels,
            "description"   =>  __('Recette de cuisine du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  12,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "recette", "with_front" => true ),
            "query_var"             => 'recette',
            "menu_icon"             => "dashicons-businessperson",
            "supports"              => array( "title" , 'editor' , 'thumbnail'  ),
        );

         register_post_type( 'recette', $args );

     }
 }

 add_action( 'init', 'enssop_cpt');
