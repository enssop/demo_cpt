<?php 

// https://fr.wordpress.org/support/article/taxonomies/
// https://developer.wordpress.org/reference/functions/register_taxonomy/

if( !defined ( 'ensssop_taxo')){
    function enssop_taxo(){
                 /**
          * Taxonomy : Type de recette
          */

          $labels2 = array(
            'name' => __( 'Types de recettes', 'enssop' ),
            'singular_name' => __( 'Type de recettes', 'enssop' ),
            'search_items' =>  __( 'Rechercher un type de recettes', 'enssop' ),
            'all_items' => __( 'Tous les types de recettes', 'enssop' ),
            'parent_item' => __( 'Type de recettes Parent', 'enssop' ),
            'parent_item_colon' => __( 'Type de recettes Parent :', 'enssop' ),
            'edit_item' => __( 'Modifier le type de recettes', 'enssop' ),
            'update_item' => __( 'Modifier le type de recettes', 'enssop' ),
            'add_new_item' => __( 'Ajouter un type de recettes' , 'enssop'),
            'new_item_name' => __( 'Nouveau type de recettes', 'enssop' ),
            'menu_name' => __( 'types de recettes' ),
          );

          $args2 = array(
              'hierarchical'    => true,
              'labels'  => $labels2,
              'show_ui' => true,
              'show_admin_column'   =>  true,
              'query_var'   => true,
              'rewrite' => array( 'slug'    =>  'recette-type'),
          );
          register_taxonomy( 'type', array('recette'), $args2);


	/**
	 * Taxonomy: Qualificatif.
	 */

	$labels = array(
		"name"          => __( "Qualificatifs", "ensssop" ),
		"singular_name" => __( "Qualificatif", "ensssop" ),
		"menu_name"     => __( "Qualificatif", "ensssop" ),
		"all_items"     => __( "Tous les Qualificatifs", "ensssop" ),
		"edit_item"     => __( "Modifier le qualificatif", "ensssop" ),
		"view_item"     => __( "Voir le Qualificatif", "ensssop" ),
	);

	$args = array(
		"label"                 => __( "Qualificatifs", "enssop" ),
		"labels"                => $labels,
		"public"                => true,
		"publicly_queryable"    => true,
		"hierarchical"          => false,
		"show_ui"               => true,
		"show_in_menu"          => true,
		"show_in_nav_menus"     => true,
		"query_var"             => true,
		"rewrite"               => array( 'slug' => 'qualificatif', 'with_front' => true, ),
		"show_admin_column"     => false,
		"show_in_rest"          => true,
		"rest_base"             => "type-de-plat",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit"    => false,
	);
	register_taxonomy( "qualificatif", array( "recette" ), $args );

        }
}

add_action('init', 'enssop_taxo');